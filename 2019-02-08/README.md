# Lego Scan


Travis CI [![Build Status](https://travis-ci.org/bgoldbeck/lscan.svg?branch=master)](https://travis-ci.org/bgoldbeck/lscan)

## Copyright ##
Copyright (C) 2018 
"Brandon Goldbeck", "Anthony Namba", "Brandon Le", "Ann Peake", "Sohan Tamang", "Theron Anderson", "An Huynh"
  
## Contact Us ##
Brandon Goldbeck: bpg@pdx.edu <br />
Anthony Namba: anamba@pdx.edu <br />
Brandon Le: lebran@pdx.edu <br />
Ann Peake: peakean@pdx.edu <br />
Sohan Tamang: sohan@pdx.edu <br />
Theron Anderson: atheron@pdx.edu <br />
An Huynh: anvanphuchuynh@gmail.com <br />

## Bug Tracker ##
https://github.com/bgoldbeck/lscan/issues

## What is this repository for? ##

* This repository contains the 2018-2019 capstone team project for computer science with professor Bart Massey.

## Setting up for Development ##

### Windows ###
...

### Linux ###

# License

...
