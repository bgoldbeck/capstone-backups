# Lego Scan


Travis CI [![Build Status](https://travis-ci.org/bgoldbeck/lego-scan.svg?branch=master)](https://travis-ci.org/bgoldbeck/lego-scan)

## Copyright ##
Copyright (C) 2018 "Brandon Goldbeck" <code>&lt;bpg@pdx.edu&gt;</code>,
  
## Contact Us ##
Brandon Goldbeck: bpg@pdx.edu <br />
Anthony Namba: anamba@pdx.edu <br />
Brandon Le: lebran@pdx.edu <br />
Ann Peake: peakean@pdx.edu <br />
Sohan Tamang: sohan@pdx.edu 

## Bug Tracker ##
https://github.com/bgoldbeck/lego-scan/issues

## What is this repository for? ##

* This repository contains the 2018-2019 capstone team project for computer science with professor Bart Massey.

## Setting up for Development ##

### Windows ###
...

### Linux ###

# License

...
